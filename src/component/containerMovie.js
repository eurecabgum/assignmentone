import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

class ContainerMovie extends Component {
  render() {
    const {title, year, img} = this.props;
    return (
      <View style={styles}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{
              height: 134,
              width: 88,
              borderRadius: 10,
              marginRight: 12,
              alignSelf: 'center',
            }}
            source={{uri: img}}
          />
          <View style={{flex: 1, flexWrap: 'wrap'}}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 14,
                color: '#cbcbcb',
                // marginTop: 12,
              }}>
              YOUR LIKES
            </Text>
            <View>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 22,
                  color: 'black',
                  marginTop: 10,
                  flexShrink: 1,
                }}>
                {title}
              </Text>
            </View>
            <View
              style={{
                backgroundColor: '#ffcc09',
                height: 22,
                width: 46,
                borderRadius: 7,
                marginTop: 8,
                marginBottom: 17,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 13,
                  color: 'white',
                  textAlign: 'center',
                  justifyContent: 'center',
                }}>
                {year}
              </Text>
            </View>
            <Text
              style={{
                fontWeight: 'normal',
                fontSize: 14,
                color: '#cbcbcb',
              }}>
              Best Movies
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default ContainerMovie;

const styles = {
  backgroundColor: '#ffffff',
  padding: 16,
  marginLeft: 16,
  marginRight: 16,
  borderRadius: 10,
  marginBottom: 11,
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 5,
  },
  shadowOpacity: 0.34,
  shadowRadius: 6.27,

  elevation: 10,
};
