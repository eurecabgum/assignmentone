import React, {Component} from 'react';
import Axios from 'axios';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import ContainerMovie from './src/component/containerMovie';

class App extends Component {
  state = {
    listFilm: [],
  };

  // componentDidMount() {
  //   Axios.get('http://www.omdbapi.com/?s=avenger&apikey=997061b4&')
  //     .then((response) => {
  //       this.setState({
  //         listFilm: response.data.Search,
  //       });
  //     })
  //     .catch((err) => console.log(err));
  // }
  async componentDidMount() {
    try {
      const response = await Axios.get(
        `http://www.omdbapi.com/?s=avenger&apikey=997061b4&`,
      );
      this.setState({
        listFilm: response.data.Search,
      });
    } catch (error) {
      alert(error);
    }
  }

  render() {
    const {listFilm} = this.state;
    return (
      <>
        <View style={styles.container}>
          <Text
            style={{
              color: 'black',
              fontSize: 32,
              fontWeight: 'bold',
              // textAlign: 'left',
            }}>
            Library
          </Text>
        </View>
        <ScrollView>
          {listFilm.map((item, index) => {
            return (
              <View key={index}>
                <ContainerMovie
                  title={item.Title}
                  year={item.Year}
                  img={item.Poster}
                />
              </View>
            );
          })}
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fafafa',
    paddingHorizontal: 16,
    paddingTop: 50,
    marginBottom: 18,
  },
});

export default App;
